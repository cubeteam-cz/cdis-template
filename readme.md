# ČD IS - frontend šablona

Jednoduchá HTML/CSS/JS šablona založená na [Twitter Bootstrap](http://getbootstrap.com)

## Instalace/spuštění

1) Nainstalovat do počítače [Node.js](https://nodejs.org/)

2) Instalace závislostí z [npm](https://www.npmjs.com)

    npm install
   
3) Spustit HTTP task pro vývoj - [Browsersync](http://www.browsersync.io)

    npm start
